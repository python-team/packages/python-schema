python-schema (0.7.7-1) unstable; urgency=medium

  * Team upload.
  * Remove patch applied upstream

 -- Alexandre Detiste <tchet@debian.org>  Sun, 15 Sep 2024 01:41:22 +0200

python-schema (0.7.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.7.6
  * remove dependency on python3-mock
  * use new dh-sequence-python3
  * add patch to work around find_package() not working

 -- Alexandre Detiste <tchet@debian.org>  Thu, 25 Apr 2024 02:12:35 +0200

python-schema (0.7.5-1) unstable; urgency=medium

  [ Ileana Dumitrescu ]
  * Team upload.
  * New upstream release (Closes: #1016647)
  * d/control: Add B/D python3-mock (fixes FTBFS)
               Bump Standards-Version to 4.6.1.
               Update dh-compat version to 13.
               Add Rules-Requires-Root line.
  * d/copyright: Update debian copyright year.

  [ Stefano Rivera ]
  * Depend on python3-mock in the autopkgtest, too.

 -- Ileana Dumitrescu <ileanadumitrescu95@gmail.com>  Sun, 04 Dec 2022 04:03:02 +0200

python-schema (0.6.7-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dpkg-dev.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 10:41:47 +0100

python-schema (0.6.7-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:41:26 -0400

python-schema (0.6.7-3) unstable; urgency=medium

  * Team upload.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Fri, 02 Aug 2019 09:20:57 +0200

python-schema (0.6.7-2) unstable; urgency=medium

  * Team upload.
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Use debhelper-compat instead of debian/compat.
  * Drop PyPy support

 -- Ondřej Nový <onovy@debian.org>  Mon, 22 Jul 2019 19:02:51 +0200

python-schema (0.6.7-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Ghislain Antony Vaillant ]
  * Update the gbp configuration
  * New upstream version 0.6.7
  * Fixup whitespacing in rules file
  * Support the nocheck build profile
  * Filter egg-info with extend-diff-ignore
  * Bump the debhelper version to 11
  * Bump the standards version to 4.1.3
  * Update the copyright years

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 22 Feb 2018 09:41:28 +0000

python-schema (0.6.6-2) unstable; urgency=medium

  * Upgrade watch file to version 4
  * Bump standards version to 4.0.0, no changes required
  * Release to unstable

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Mon, 10 Jul 2017 10:50:16 +0100

python-schema (0.6.6-1) experimental; urgency=medium

  * New upstream version 0.6.6
  * Drop superfluous Testsuite field
  * Upgrade the packaging to debhelper 10
  * Run the DEP-8 tests for all supported Python versions
  * Enable testing for PyPy
    - Add pypy-pytest to Build-Depends
    - Add DEP-8 test command for PyPy
    - Let pybuild run the tests for PyPy
  * Fixup the Vcs-Browser URI
  * Fixup the long descriptions
  * Release to experimental

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 06 May 2017 15:45:25 +0100

python-schema (0.6.5-1) unstable; urgency=medium

  * New upstream version 0.6.5
  * Drop Add-upstream-testsuite.patch, no longer required.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Tue, 27 Sep 2016 07:52:27 +0100

python-schema (0.6.2-1) unstable; urgency=low

  * Initial release. (Closes: #832976)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sun, 31 Jul 2016 20:54:55 +0100
